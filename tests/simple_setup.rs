#[cfg(test)]
mod test {
    use ed25519_dalek::SecretKey;
    use rand::rngs::OsRng;
    use tapir_cwtch::acns::tor::authentication::HashedPassword;
    use tapir_cwtch::acns::tor::TorProcess;
    use tapir_cwtch::connections::service::Service;
    use tapir_cwtch::primitives::identity::Identity;

    use tapir_cwtch::applications::authentication_app::AuthenicationApp;
    use tapir_cwtch::connections::{Connection, InboundConnection, OutboundConnection};
    use tapir_cwtch::primitives::transcript::Transcript;

    #[test]
    fn test_simple_setup() {
        let mut auth_control_port = TorProcess::connect(9051)
            .unwrap()
            .authenticate(Box::new(HashedPassword::new(String::from("examplehashedpassword"))))
            .unwrap();
        let mut csprng = OsRng {};
        let keypair = ed25519_dalek::Keypair::generate(&mut csprng);
        match auth_control_port.add_onion_v3(SecretKey::from_bytes(&keypair.secret.to_bytes()).unwrap(), 9878, 10029) {
            Ok(service_id) => {
                // we authenticated!
                let identity = Identity::initialize(keypair);
                println!("Service Id: {}", service_id);
                println!("Setup: {}", identity.hostname());

                let service = Service::init(identity.clone());

                let outbound_identity = identity.clone();
                let outbound_service = |mut conn: Connection<OutboundConnection>| {
                    let mut transcript = Transcript::new_transcript("tapir-transcript");
                    let mut auth_app = AuthenicationApp::new(outbound_identity);
                    match auth_app.run_outbound(conn, &mut transcript) {
                        Ok(_conn) => {
                            println!("Authenticated");
                        }
                        Err(err) => {
                            println!("Error: {:?}", err);
                        }
                    }
                };

                let identity = identity.clone();
                let inbound_service = |mut conn: Connection<InboundConnection>| {
                    let mut transcript = Transcript::new_transcript("tapir-transcript");
                    let mut auth_app = AuthenicationApp::new(identity);
                    auth_app.run_inbound(conn, &mut transcript);
                };

                let mut service = service.listen(10029, inbound_service.clone()).unwrap_or_else(|_| panic!());

                match service.connect("yxdappsm5v45njecytgok7j2lwbsxkcfbtuvfkfrx7l2jqf2a73n47yd", outbound_service.clone()) {
                    _ => {}
                }
                service.close();
                loop {}
            }
            Err(err) => println!("{:?}", err),
        }
    }
}

#![feature(array_methods)]
// #[deny(missing_docs)]
#![feature(external_doc)]
#![doc(include = "../README.md")]
#![doc(html_logo_url = "https://git.openprivacy.ca/cwtch.im/tapir/media/branch/master/tapir.png")]

pub mod acns;
pub mod applications;
pub mod connections;
pub mod primitives;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

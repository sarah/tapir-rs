use crate::acns::ACNError;
use crate::acns::ACNError::AuthenticationError;
use ed25519_dalek::{PublicKey, PUBLIC_KEY_LENGTH};
use sha3::Digest;

/// check if the given hostname is a valid onion v3 address
pub fn validate_hostname(hostname: &str) -> bool {
    match base32::decode(base32::Alphabet::RFC4648 { padding: false }, hostname) {
        Some(hostname_bytes) => hostname == public_key_to_hostname(&PublicKey::from_bytes(hostname_bytes.split_at(PUBLIC_KEY_LENGTH).0).unwrap_or_default()),
        _ => false,
    }
}

/// convert a tor onionv3 hostname to a public key
pub fn hostname_to_public_key(hostname: &str) -> Result<PublicKey, ACNError> {
    match base32::decode(base32::Alphabet::RFC4648 { padding: false }, hostname) {
        Some(hostname_bytes) => Ok(PublicKey::from_bytes(hostname_bytes.split_at(PUBLIC_KEY_LENGTH).0).unwrap_or_default()),
        _ => Err(AuthenticationError(String::from("error decoing hostname"))),
    }
}

/// convert the given public key to a tor onion v3 hostname
pub fn public_key_to_hostname(public_key: &PublicKey) -> String {
    let mut buf = [0u8; 35];
    public_key.to_bytes().iter().copied().enumerate().for_each(|(i, b)| {
        buf[i] = b;
    });

    let mut h = sha3::Sha3_256::new();
    h.update(b".onion checksum");
    h.update(public_key);
    h.update(b"\x03");
    let res_vec = h.finalize().to_vec();
    buf[32] = res_vec[0];
    buf[33] = res_vec[1];
    buf[34] = 0x03;
    base32::encode(base32::Alphabet::RFC4648 { padding: false }, &buf).to_ascii_lowercase()
}

#[cfg(test)]
mod tests {
    use crate::acns::tor::validation::{hostname_to_public_key, validate_hostname};
    use crate::primitives::identity::Identity;
    use std::process::id;

    #[test]
    fn test_validation() {
        let identity = Identity::initialize_ephemeral_identity();
        let hostname = identity.hostname();
        assert!(validate_hostname(hostname.as_str()));
        assert_eq!(hostname_to_public_key(hostname.as_str()).unwrap(), identity.public_key())
    }
}

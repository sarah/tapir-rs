use std::io::Write;
use std::net::TcpStream;

pub trait TorAuthenticationMethod {
    fn authenticate(&self, conn: &mut TcpStream);
}

pub struct HashedPassword {
    password: String,
}

impl HashedPassword {
    pub fn new(password: String) -> HashedPassword {
        HashedPassword { password }
    }
}

impl TorAuthenticationMethod for HashedPassword {
    fn authenticate(&self, conn: &mut TcpStream) {
        match write!(conn, "AUTHENTICATE \"{}\"\r\n", self.password) {
            _ => {} // If anything bad happens we will know soon enough...
        }
    }
}

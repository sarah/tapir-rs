pub mod tor;

#[derive(Debug)]
pub enum ACNError {
    ConfigurationError(String),
    AuthenticationError(String),
    ServiceSetupError(String),
}
